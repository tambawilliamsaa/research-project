import numpy as np
import matplotlib.pyplot as pl
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt

tau = 30e-15 # in s
w0  = 3.55e15 # in Hz
w   = [3.35e15,3.75e15, 0.001e15]

S   =[ tau*np.sqrt(np.pi)/2 *np.exp(-((tau/2*(w-w0))**2))]

plot(w,S)
xlabel('frequency \omega (Hz)' )
ylabel('intensity I (a.u.)')
