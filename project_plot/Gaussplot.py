import numpy as np
import matplotlib.pyplot as pl
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt

tau = 30*10**(-15) # in s
w0  = 3.55*10**15 # in Hz
w   = np.arange(3.35*10**15,3.75*10**15, 0.00001*10**15)
S= (tau/2)*np.sqrt(np.pi)*np.exp(-(((tau/2)*(w-w0))**2))


pl.plot(w,S)
pl.xlabel('frequency (Hz)' )
pl.ylabel('intensity I (a.u.)')
pl.show()

