import numpy as np
import matplotlib.pyplot as pl
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt


# making the plots
data = np.loadtxt('530nm_pulse_spectrum.txt')


x=data[:,0]
y=data[:,1]

def gauss(x,a,b,c,d):
    y = a*np.exp(-(x-b)**2/(2*c**2)) +d
    return y

initial_values = np.array([1,530,-7.0706,0])

pl.plot(x,y,'r+')
pl.axhline(y=21466.810000000001, xmin= 0, xmax=885.22000000000003, color='g', ls='dashed')
fitParams, fitCovariances = curve_fit(gauss,x,y,p0=initial_values)
pl.plot(x, gauss(x, fitParams[0], fitParams[1], fitParams[2], fitParams[3]))
pl.xlabel('wavelength($nm$)')
pl.ylabel('intensity(arbitrary units)')
plt.xticks(np.arange(min(x), max(x)+1, 100))
pl.show()
print fitParams

