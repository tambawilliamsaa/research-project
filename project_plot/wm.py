import numpy as np
import matplotlib.pyplot as pl
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt

tau = 30e-15 # in s
u0  = 530e-9 # in Hz
u   =np.arange(505e-9, 555e-9, 0.001e-9)

S   = tau*np.sqrt(np.pi)/2 * np.exp(-(tau*np.pi*3e8*(1/u0 - 1/u))**2 ) 

pl.plot(u,S)
pl.xlabel('wavelength (nm)' )
pl.ylabel('intensity I (a.u.)')
pl.show()
