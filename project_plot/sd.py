import numpy as np
import matplotlib.pyplot as pl

from scipy.optimize import curve_fit
from scipy.interpolate import spline

#from math import exp
#from array import *
#from scipy.stats import norm
#import matplotlib.pyplot as plt
#import scipy.stats as stats
#import datetime


# making the plots
data = np.loadtxt('frequency.txt')
g2d = pl.plot(data[:,0],data[:,1],'r+', label='signal')
pl.xlim(-75,75)
#pl.xlabel('$time(\textbf{fs})$')
#pl.ylabel('intensity(arbitrary units)')
#pl.show()

x=data[:,0]
y=data[:,1]

def gauss(x,a,b,c,d):
    y = a*np.exp(-(x-b)**2/(2*c**2))+d
    return y

#plot horizontal lines
x1=[-22.1546,-22.1546]
y1=[0,0.47935215958196009]
pl.plot(x1,y1, 'g--')
x2=[20.4668,20.4668]
y2=[0,0.47935215958196009]
pl.plot(x2,y2, 'g--')

##plot vertical line
pl.axhline(y=0.47935215958196009, xmin= 0, xmax=250.63379399999999, color='k', ls='dashed')


fitParams, fitCovariances = curve_fit(gauss,x,y)
y1=gauss(x, fitParams[0], fitParams[1], fitParams[2], fitParams[3])
pl.plot(x, y1, label='fit')
pl.xlabel('time(fs)')
pl.ylabel('intensity(arbitrary units)')

#including the legend in the plot.
pl.legend()
#pl.ion()
pl.show()
print fitParams
#print fitCovariances

#s=np.std(data[:,1])
#print s






