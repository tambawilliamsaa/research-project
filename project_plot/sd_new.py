import numpy as np
import matplotlib.pyplot as plt

from scipy.optimize import curve_fit
from scipy.interpolate import spline

data = np.loadtxt('frequency.txt')

x=data[:,0]
y=data[:,1]

def gauss(x,a,b,c,d):
    y = a*np.exp(-(x-b)**2/(2*c**2))+d
    return y

fitParams, fitCovariances = curve_fit(gauss,x,y)
plt.plot(x, gauss(x, fitParams[0], fitParams[1], fitParams[2], fitParams[3]))

xnew = np.linspace(x.min(),x.max(),500)
y_smooth = spline(x,y,xnew)
#plt.plot(xnew,y_smooth)
plt.ion()
plt.show()

x[list(y).index(30)]
s = xnew[56] 
print s, y_smooth[list(xnew).index(s)]
