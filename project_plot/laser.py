## LIBRARIES
import numpy as np
import pylab as pl
from math import exp

# Opening the data 
data = np.loadtxt('frequency.txt')

# making the plots

pl.plot(data[:,0],data[:,1],'r+')
pl.xlim(-300,260)
pl.xlabel('time(fs)')
pl.ylabel('intensity(arbitrary units)')
pl.show()
a=np.std(data[:,0])
b=np.std(data[:,1])
print a, b

######################################################################
######################################################################
######################################################################

# 
## Opening the data 
data1 = np.loadtxt('530nm_pulse_spectrum.txt')

# making the plots

pl.plot(data1[:,0],data1[:,1],'g+')
pl.xlabel('wavelength(nm)')
pl.ylabel('intensity(arbitrary units)')
pl.show()

######################################################################
######################################################################
######################################################################

#import matplotlib.pyplot as plt

#T=30*10^-15
#c = 299792458 
#l = 530*10^-9
#f = c/l
#w=2*np.pi*f
#t = np.arange(-40*10^-15,40*10^-15,10^-15)
#C = np.cos(w*t)
#a = np.exp(-(BigFloat.div(t**2,T**2)))*C

#plt.plot(t,a)
#plt.show()

######################################################################
######################################################################
#########







