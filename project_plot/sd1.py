import numpy as np
import matplotlib.pyplot as pl
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt


# making the plots
data = np.loadtxt('530nm_pulse_spectrum.txt')


x=data[:,0]
y=data[:,1]

def gauss(x,a,b,c,d):
    y = a*np.exp(-(x-b)**2/(2*c**2)) +d
    return y
initial_values = np.array([1,530,-7.0706,0])
pl.plot(x,y,'r+', label='signal')

#plot horizontal lines
x1=[520.403,520.403]
y1=[0,20810.734759700754]
pl.plot(x1,y1, 'g--')
x2=[538.38,538.38]
y2=[0,20810.734759700754]
pl.plot(x2,y2, 'g--')

#plot vertical line
pl.axhline(y=20810.734759700754, xmin= 0, xmax=885.22000000000003, color='k', ls='dashed')

#fitting the data to a Gaussian
fitParams, fitCovariances = curve_fit(gauss,x,y,p0=initial_values)
y1=gauss(x, fitParams[0], fitParams[1], fitParams[2], fitParams[3])
pl.plot(x, y1, label='fit')
pl.xlabel('wavelength($nm$)')
pl.ylabel('intensity(arbitrary units)')
plt.xticks(np.arange(min(x), max(x)+1, 100))
#including the legend in the plot.
plt.legend()

#displaying the plot.
pl.show()
print fitParams





